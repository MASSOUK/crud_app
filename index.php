<?php  
	session_start();
	// $_SESSION=array();
?>
<?php 
if (isset($_SESSION['USER'])) {
	 	if($_SESSION['USER']['niveau']==5){		
			header('location:pages/master_account.php');			
		}else{
			header('location:pages/user_account.php');
		}
}
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	
</head>
<body class="corps ">
	<div class="container-fluid ">
		<?php 
			// print_r($_SERVER);
		?>
		<div class="row  navbar navbar-default" style="padding-right: 2%; background: inherit !important; border: none;">                    
			<a href="pages/inscription.php">
				<input type="submit" value="Inscription" placeholder="Inscription" class="form-control pull-right" style="width:90px;margin-left: 30px; margin-top: 10px; background: #c850c0;"> 
			</a>                     
			<a href="pages/connexion.php">
				<input type="submit" value="Connexion" placeholder="Connexion" class="form-control pull-right" style="width:90px;margin-top: 10px;"> 
			</a>                 
		</div>		
		
		<div class="row ">
			<h2></h2>
			<div class="forme col-md-offset-2 col-md-8  col-xs-12 col-xm-offset-2 col-xm-10">
				<h3 class="libeleindex">CONNEXION / INSCRIPTION</h3>

				<div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6 ">
					<a href="pages/connexion.php">
						<input  type="submit"  class="  btn btn-block btn-info" value="Connexion">
					</a> 
				</div>

				<div class="  col-md-4 col-xm-5 col-xs-6">
					<a href="pages/inscription.php">
						<input  type="submit"  class="  btn btn-block btn-info" value="Inscription">
					</a>
				</div>
						
			</div>
		</div>
		
	</div>
	</body>
	<!-- <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script> -->
	<script type="text/javascript">
    var loadFile = function(event) {
        var profil = document.getElementById('im');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
    var	nom = document.getElementById('name');
    var prenom=document.getElementById('prenom');
    var pwd=document.getElementById('pwd');
      
   


	nom.addEventListener('change', testnom); 
	function testnom(){
	 	if(nom.value.length<4){ 
	 		nom.setCustomValidity('Veuillez entrer un nom comprenant au moins 4 caracteres');
			return false;
		}
		else if(contient_specialchart(nom.value)){ 
			nom.setCustomValidity('Le nom doit avoir au minimum 4 lettres sans les caracteres speciaux tels que @ ou % ...');
			return false;
		}else{
			return true;
		}
	}


	prenom.addEventListener('change', testprenom); 
	function testprenom(){		
	 	if(prenom.value.length<3){ 
	 		prenom.setCustomValidity('Veuillez entrer un prenom comprenant au moins 4 caracteres');	  
			return false;
		}
		else if(contient_specialchart(prenom.value)){  
			prenom.setCustomValidity('Le prenom doit avoir au minimum 4 lettres sans les caracteres speciaux tels que @ ou % ...');
			return false;
		}else{
			return true;
		}
	}
	function contient_specialchart(char){
		var special = ['@','!','~','<','>','?',' " '," ' ",'*','(',')','^','%','$','#','&','{','}','[',']',';'];

		for (var i = special.length - 1; i >= 0; i--) { 
			for (var id in char) {  
				if(char[id]==special[i]){
					return true;
				}
			}
		}
		return false;
	}

	pwd.addEventListener('change', testpwd);
	function testpwd(){
		if(pwd.value.length<8){
			// pwd.setCustomValidity('Veuillez entrer un mot de passe comprenant au moins 8 lettres et caracteres speciaux');
			return false;
		}else{
			return true;
		}

	}



  </script>
  


</html>