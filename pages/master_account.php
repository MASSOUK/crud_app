<?php  
	session_start();
	if (!isset($_SESSION['USER'])) {
		header('location: connexion.php');
	}
?>

<?php  
	include('header_account.php');
?>
<?php 
	// on ajoute un indicateur dans la session pour specifier que c'est l'admin qui opere
	$_SESSION['admin']='ok'; 
?>
	<style type="text/css">
		.faire{
			display: inline-block;
			margin: 15px, 15px;
		}
		.nomination{
			max-width: 80px;
			word-wrap: break-word;
		}
		.ligne_active:hover{
			background-color: gainsboro;
		}
	</style>

	<div class="col-md-6 col-md-offset-3" style="border: 1px solid;">
		<h2 style="text-align: center; font-weight: bold;">ETAT DES MEMBRES ACTIFS</h2>
	<?php  

		// print_r($_SESSION);
		    
	?>
<?php  
	// connection a la bdd
	$bdd = new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
	$response=$bdd->query('SELECT * FROM utilisateur');


?>	
	<div class="table-responsive">
		<!-- Table -->
		<table class="table">
			<tr class="bg-primary">
				<td>
					Id
				</td>
				<td class="nomination">
					Nom
				</td>
				<td class="nomination">
					Prenom
				</td>
				<td>
					photo
				</td>
				<td>
					Etat
				</td>
				<td style="text-align: center;">
					Actions
				</td>
				<!-- <td>
					Photo
				</td> -->
			</tr>

			<?php 
			$i=0;
			while ($donnees =$response->fetch()) { 
				if ($donnees['niveau']=='5') {
					continue; // on n'affiche pas les administrateurs	
				}
				$i++;
				$marker=$donnees['id'];?>

				<tr class="ligne_active">
					<td class="bg-primary">
						<?php echo $i; ?>					
					</td>
					<td class="nomination">
						<?php echo $donnees['nom']; ?>
					</td>
					<td class="nomination">
						<?php echo $donnees['prenom']; ?>
					</td>
					<td>
						<div style="width: 25px; height: 25px; background-image: url(../images/<?php echo $donnees['photo']; ?> ); background-size: cover;" >	
						</div>
					</td>
					<td>
						<?php

							switch ($donnees['niveau']) {
								case '1':
									$state='Actif';
									$mes='Bloquer';
									echo '<span style="color: green;">'.$state.'</span>';
									break;
								case '0':  
									$state='Supprime';
									$mes=' Activer';
									echo '<span style="color: red;">'.$state.'</span>';
									break;
								case '2':
									$state='Bloque';
									$mes=' Activer';
									echo '<span style="color: goldenrod;">'.$state.'</span>';
									break;
								default:
									$mes='';
									break;
							}
						?>
					</td>
					<td style="padding-left: 50px;">
						<form class="faire" method="POST" action="modif.php">
							<input type="hidden" name="id" value="<?php echo $marker; ?>">
							<button type="submit" style="color: blue;" title="Modifier?"><span class="glyphicon glyphicon-pencil"></span></button>
						</form>
						<?php
							// echo $marker;
							switch ($state) {
								case 'Actif': ?>
									<form class="faire" method="POST" action="testmodif.php">
										<input type="hidden" name="do" value="<?php echo $state ?>">
										<input type="hidden" name="id" value="<?php echo $marker; ?>">
										<button type="submit" style="color: goldenrod;" title="Bloquer?"><span class="glyphicon glyphicon-eye-close"></span></button>
									</form>

									<form class="faire" method="POST" action="testmodif.php">
										<input type="hidden" name="id" value="<?php echo $marker; ?>">
										<input type="hidden" name="do" value="remove">
										<button type="submit" style="color: red;" title="Supprimer?"><span class="glyphicon glyphicon-remove"></span></button>
									</form>

								<?php   
									break;
								case 'Bloque': ?>
									<form class="faire" method="POST" action="testmodif.php">
										<input type="hidden" name="do" value="<?php echo $state ?>">
										<input type="hidden" name="id" value="<?php echo $marker; ?>">
										<button type="submit" style="color: green;" title="Activer?"><span class="glyphicon glyphicon-eye-open"></span></button>
									</form>

									<form class="faire" method="POST" action="testmodif.php">
										<input type="hidden" name="id" value="<?php echo $marker; ?>">
										<input type="hidden" name="do" value="remove">
										<button type="submit" style="color: red;" title="Supprimer?"><span class="glyphicon glyphicon-remove"></span></button>
									</form>

								 <?php   
									break;
								case 'Supprime': ?>
									<form class="faire" method="POST" action="testmodif.php">
										<input type="hidden" name="do" value="<?php echo $state ?>">
										<input type="hidden" name="id" value="<?php echo $marker; ?>">
										<button type="submit" style="color: green;" title="Activer?"><span class="glyphicon glyphicon-eye-open"></span></button>
									</form>

								 	<?php  
									break;
								default:
									break;
							}

							?>
						
					</td>
				</tr>
			<?php } ?>  	
		</table>

	</div>

	</div>
