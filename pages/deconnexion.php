
<?php  
	session_start();
?>

<?php  
	if(isset($_SESSION['USER'])){
		session_destroy();
	}
	
	header('location: ../index.php');

?>