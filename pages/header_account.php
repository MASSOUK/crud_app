<?php// session_start(); ?>
 
 <?php  
  if(isset($_SESSION)){
  }else{
    header('location: ../index.php');
  }
  
?>   

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="stylesheet" href="../css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/headeraccount.css">
  <title></title>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header" style="position: absolute; top: 0px; left: 3%;">
      <a class="navbar-brand" href="user_account.php"><span class="glyphicon glyphicon-home center">Acceuil</span></a>
    </div>

    <ul class="nav navbar-nav navbar-right" style="position: absolute; top: 0px; right: 1%;">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" > <span class="user_name"><?php echo $_SESSION['USER']['nom']; ?></span><?php echo '<img src="../images/'.$_SESSION['USER']['photo'].'" style="width:30px; height:30px; border-radius: 50%;">' ?> </a>
        <ul class="dropdown-menu">
          <?php if ($_SESSION['USER']['niveau']!='2') { ?>
            <li><a href="moncompte.php">Profil</a></li>  
          <?php }else{ ?> 
            <li><a id="alerter" href="#" style="color: goldenrod;">Restriction</a></li>  
          <?php } ?>
          <li><a href="#myModal" data-toggle="modal">Deconnexion</a></li>
        </ul>
      </li>
    </ul>
  </div><!-- /.container-fluid -->
</nav>

<div class="container"> 
  <div id="afficheur" class="row" style="display: none;">
    <div class="col-md-4 col-md-push-4">
      <div class="alert alert-danger" style="margin: 20px; text-align: center;">
        <button class="pull pull-right" data-dismiss="alert"><span class="glyphicon glyphicon-eye-close"></span></button>     <!-- data-dismiss fait disappear l'element en parametre -->
      Vos Acces ont ete restrints. Veuillez contacter votre gestionnaire.</div>   <!-- alert-danger, alert-success, alert- -->
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" style="width: 20%; margin-left: 40%; margin-top: 10%;">
  <div class="modal-content">
    <div class="modal-header">
      <span>Voulez vous vraiment Quitter?</span>
      <button class="close btn btn-danger" data-dismiss="modal" style="background-color: red; padding: 5px;"><span class="glyphicon glyphicon-remove"></span></button>
    </div>
    <a href="deconnexion.php">
      <button class="btn btn-default btn-lg" style="margin-left: 45%;"><span style="color:green;" class="glyphicon glyphicon-ok"></span></button>
     </a>
  </div>
</div>

<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
  var profil=document.getElementById('alerter');
  var zone=document.getElementById('afficheur');
      profil.addEventListener('click', presente);
      function presente(){
        zone.style.display='block';
      }

</script>
</body>
</html>



