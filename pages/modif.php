<?php  
	session_start();
?>

<?php  
	include('header_account.php');
?>

<?php

	if (!empty($_POST['id'])) {

		$id=$_POST['id'];

		$bdd = new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
   		$dataRecup=$bdd->prepare('SELECT * FROM utilisateur WHERE id = ?');
		$dataRecup->execute(array($_POST['id']));

		$user=$dataRecup->fetch();

?>
	<style type="text/css">
		.img_visualise{
			width: 100px; 
			height: 100px; 
			border-radius: 50%;
		}
		.load_img{
			position: relative; top: -25px; left: 85px;
			/*z-index: 2;*/
			border: 1px solid;
			width: 25px;
			height: 25px;
			border-radius: 50%;
			/*background-color: blanchedalmond;*/
			background-color: #f8f8f8;
		}
		.fa-refresh{
			position: relative; left: 5px; top: 2px;
		}
		.libele{
			/*border: 1px solid;*/
			display: inline-block;
			width: 70px;
		}
		#new_image{
			/*opacity: 0;*/
			height: 0px;
		}
	</style>
	<div class="col-md-8 col-md-push-1 col-sm-8 c0l-sm-push-1" style="border: 1px solid;">
		<h3>Informations De l'utilisateur:</h3>

		<form action="traitement_modif.php" method="POST" enctype="multipart/form-data">
			<!-- <form action="" method="POST" enctype="multipart/form-data"> -->
			 <!-- <label for="new_image" class="load_img"  title="Editer"><span class="fa fa-refresh"></span></label> -->
		    <label for="new_image"><img id="image_depart" class="img_visualise" <?php echo "src='../images/".$user['photo']."'" ?>></label>
			<input id="new_image" type="file" name="photo" accept="image/png, image/jpeg, image/jpg" value='<?php echo $user['photo']?>'>
			
			<label> 
				<label for="nom" class="btn btn-xs btn-default pull-right indicate"><span class="fa fa-pencil"></span></label> 
				<span class="libele">Nom :</span> 
				<input id="nom" style="background-color: inherit; border: none;" type="text" name="nom" maxlength="255" value=<?php echo "'".$user['nom']."'" ?>>
			</label> <br>

			<label>
				<label for="prenom" class="btn btn-xs btn-default pull-right indicate"><span class="fa fa-pencil"></span></label>
				<span class="libele">Prenom :</span>
				<input id="prenom" style="background-color: inherit; border: none;"  type="text" name="prenom" maxlength="255" value=<?php echo "'".$user['prenom']."'" ?>>
			</label> <br>

			<label>
				<!-- <label for="email" class="btn btn-xs btn-default pull-right indicate"><span class="fa fa-pencil"></span></label> -->
				<span class="libele">Email :</span>
				<input id="email" style="background-color: inherit; border: none;"  type="email" name="email" disabled="" value=<?php echo "'".$user['email']."'" ?>>
			</label> <br>

			<input type="hidden" name="id" value="<?php echo $_POST['id'] ?>">
			<input type="reset" value="Annuler" class="btn btn-default btn-sm">	<input type="submit" value="Confirmer" class="btn btn-default btn-sm">
		
		</form>

	</div>

	<script type="text/javascript">
		var bouton = document.getElementById('edit'),
			input = document.getElementsByTagName('input'),
			label = document.getElementsByClassName('indicate'),
			imgProfil = document.getElementById('image_depart');

			// console.log(input);
		var Zimage = input[0], Znom = input[1], Zprenom = input[2], Zmail = input[3], reset = input[5], submit = input[6];

		//Situation de depart
		function cache(){  // Mode consultation des infos
			console.log(Zimage);
			Zimage.previousElementSibling.removeChild(Zimage.previousElementSibling.firstChild);
			Zimage.previousElementSibling.appendChild(imgProfil);
		}

		function affiche (){  // Mode Edition des infos
			Zimage.previousElementSibling.setAttribute('title','Editer');
			for (var i = label.length - 1; i >= 0; i--) {
				label[i].style.display='block';
				label[i].setAttribute('title','Editer');
			// label[i].style.color='red';
			}
		}

		function view_newimage(){
			// Supprimons le contenu de la zone d'affichage
			while(Zimage.previousElementSibling.firstChild) {
				Zimage.previousElementSibling.removeChild(Zimage.previousElementSibling.firstChild);
			}
			//
			var image = document.createElement('img'); //on cree in noeud, un element, une balise image
			image.setAttribute('class','img_visualise'); // on attribue a notre balise une classe
			image.src = URL.createObjectURL(Zimage.files[0]); // on recupere le chemin indicant la source de notre image
			Zimage.previousElementSibling.appendChild(image); // on definit notre image comme element enfant de sa zone d'affichage soit on positionne notre noeud dans le dom
		}

		affiche();
		Zimage.addEventListener('change', view_newimage);
		reset.addEventListener('click', cache); // on reviens en mode consultation.
	</script>

<?php

	}else{
		header('location:master_account.php');
	}

?>