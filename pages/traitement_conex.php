
<?php  
	session_start();
?>


<?php  
	// verification de reception des donnees et appropriation de celles ci
	if(isset($_POST)){
		if (isset($_POST['email']) && isset($_POST['pwd'])) {
			$mail = $_POST['email'];
			$pwd = $_POST['pwd'];
		// Verification d'existence de l'adresse mail
			// Connection a la BDD
	    	$bdd= new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

	    	// Recuperer le champ ayant la valeur de mail correspondant
	        $dataRecup=$bdd->prepare('SELECT * FROM utilisateur WHERE email = ?');
	        $dataRecup->execute(array($mail));

	    	if ($response=$dataRecup->fetch()) {	//si email existe dans la base
	    		$_SESSION = array();
	    		$_SESSION['USER']=$response;
	    		// echo('utilisateur existant');

	    		// verification de correspondance du pwd 
	    		if ($_SESSION['USER']['pwd']==$pwd) {
	    			// Controle des niveaux d'access et redirection vers la page utilisateur.
	    			// if($_SESSION['USER']['niveau']=='5'){
	    			// 	header('location:master_account.php');
	    			// }else{
	    			// 	header('location:user_account.php');    				
	    			// }
	    			if (isset($_POST['admin'])) {
	    				header('location:master_account.php');
	    			}else{
		    			switch ($_SESSION['USER']['niveau']) {
		    				case '5':
		    					$_SESSION = array();
		    					$_SESSION['message_error']="Cet Utilisateur n'existe pas";
		    					header('location:connexion.php');
		    					break;

		    				case '0':
		    					$_SESSION = array();
		    					$_SESSION['message_error']='Acces refusé, Contactez votre gestionnaire';
		    					header('location:connexion.php');
		    					break;
		    				
		    				default:
		    					header('location:user_account.php');
		    					break;
		    			}
	    			}
	    		}else{
	    			$_SESSION['message_error']='Mot de passe incorrect';
	    			header('location:connexion.php');
	    		}
	    	}else{
	    		$_SESSION = array();
	    		$_SESSION['message_error']='Compte inexistant';
	    		header('location:connexion.php');
	    		// echo('utilisateur non existant');
	    	}
   		}
	}
	 
	

?>